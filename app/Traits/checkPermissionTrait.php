<?php

namespace App\Traits;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

trait checkPermissionTrait
{

    public function can_do($permission)
    {
      
        $auth_user_id = Auth::id();

        $user = User::findOrfail($auth_user_id);

        if ($user->can($permission) == false) {

            abort(401);
        }
    }
}
