<?php

namespace App\Http\Controllers\API;

use App\Exceptions\categoryException;
use App\Models\Category;
use App\Models\Item;
use App\Models\Menu;
use App\Models\Discount;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
class ItemController extends Controller
{

    public function ValidationRules($rules = [])
    {
        return array_merge([
            "name" => ['required', 'string'],
            'price' => ['required','numeric'],
            'category_id' => ['exists:categories,id'],
        ], $rules);
    }
   
    
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function listItem()
    {
        $menu_ids=Menu::select('id')->where('user_id',auth()->user()->id)->get();
        $categories_id=Category::select('id')->whereIn('menu_id',$menu_ids)->get()->pluck('id');
        $result=Item::whereIn('category_id',$categories_id)->with('category.discounts')->get();
        return new JsonResponse($result, 201);
  
    }



    public function store(Request $request)
    {
        $data = $request->all();
        Validator::make($data, $this->ValidationRules())->validate();
        // if( count(Category::where('id',$data['category_id'])->first()->parentRecursiveFlatten())>3){
        //     return  throw new categoryException;
        // }
         $item=Item::create($data);
        return new JsonResponse($item, 200);
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $item = Item::findOrFail($id);    
        return new JsonResponse($item, 200);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $validated=Validator::make($data, $this->ValidationRules())->validate();
        $item = Item::findOrFail($data['id']);
        $item?->update($validated);
        return new JsonResponse( ['message' => __('updated successfully')], 200);
   
    }

   
    public function destroy(Request $request)
    {
        $data = $request->all();
        Validator::make($data, [
            'id' => ['required', 'exists:items,id'],
        ], [
            'id' => __('invalid record selected')
        ])->validate();
 
        Item::destroy($data['id']);
        return  response()->json([
                'message' => __("deleted successfully")
            ], 200);
      
    }
   
}
