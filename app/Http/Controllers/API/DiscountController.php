<?php

namespace App\Http\Controllers\API;


use App\Models\Discount;
use App\Models\Item;
use App\Models\Category;
use App\Services\DiscountServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
class DiscountController extends Controller
{
    private DiscountServices $discoutService;
 
    public function __construct(DiscountServices $discoutService)
    {
        $this->discoutService = $discoutService;
    }

    public function ValidationRules($rules = [])
    {
        return array_merge([
            "name" => ['required', 'string'],
            "value" => ['required','numeric'],
            "status" => ['required', 'string'],
            "type" => ['required', 'string'],
            "application" => ['required', 'string'],
            "model_id" => ['required'],
          
        ], $rules);
    }
   
  /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function listDiscount()
    {
        $result=Discount::where('user_id',auth()->user()->id)->with('modelable')->get();
        return new JsonResponse($result, 200);
  
    }

    public function store(Request $request)
    {
        $data = $request->all();
        Validator::make($data, $this->ValidationRules())->validate();
        switch ($data['application']) {
            case "All Menu":
                $this->discoutService->discoutMenue($data);
            break;
            case "Category/Subcategory":
                $this->discoutService->discoutCategory($data);
            break;
            case "Item":
                $this->discoutService->discoutItem($data);
            break;
           
        }
        
        return new JsonResponse("success", 200);
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $item = Discount::findOrFail($id);    
        return new JsonResponse($item, 200);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $validated=Validator::make($data, $this->ValidationRules())->validate();
        $item = Discount::findOrFail($data['id']);
        $item?->update($validated);
        return new JsonResponse( ['message' => __('updated successfully')], 200);
   
    }

     
    public function delete(Request $request)
    {
        $data = $request->all();
        Validator::make($data, [
            'id' => ['required', 'exists:discounts,id'],
        ], [
            'id' => __('invalid record selected')
        ])->validate();
 
        Discount::destroy($data['id']);
        return  response()->json([
                'message' => __("deleted successfully")
            ], 200);
      
    }
   
}
