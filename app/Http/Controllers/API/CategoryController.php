<?php

namespace App\Http\Controllers\API;

use App\Models\Discount;
use App\Models\Item;
use App\Models\Category;
use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
class CategoryController extends Controller
{

    public function ValidationRules($rules = [])
    {
        return array_merge([
            "name" => ['required', 'string'],
            'menu_id' => ['required', 'exists:menus,id'],
            'category_id' => ['exists:categories,id'],
        ], $rules);
    }
   
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function listCategory()
    {
        $menu_ids=Menu::select('id')->where('user_id',auth()->user()->id)->get();
        $result=Category::whereIn('menu_id',$menu_ids)->with(['menu:id,name','parent:id,name'])->get();
        return new JsonResponse($result, 200);
  
    }

    public function listAllLevelCategory()
    {
        $menu_ids=Menu::select('id')->where('user_id',auth()->user()->id)->get();
        $result=Category::whereIn('menu_id',$menu_ids)->whereNull('category_id')->with(['childrenRecursive','items'])->get();
        return new JsonResponse($result, 200);
  
    }

    public function store(Request $request)
    {
        $data = $request->all();
        Validator::make($data, $this->ValidationRules())->validate();
         $category=Category::create($data);
         $category = Category::findOrFail($category->id);  
        return new JsonResponse($category, 200);
    }

    public function edit(Request $request)
    {
        $id = $request->id;
        $category = Category::findOrFail($id);    
        return new JsonResponse($category, 200);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $validated=Validator::make($data, $this->ValidationRules())->validate();
        $category = Category::findOrFail($data['id']);
        $category?->update($validated);
        return new JsonResponse( ['message' => __('updated successfully')], 200);
   
    }

       /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return response json
     */
    public function destroy(Request $request)
    {
        $data = $request->all();
        Validator::make($data, [
            'id' => ['required', 'exists:categories,id'],
        ], [
            'id' => __('invalid record selected')
        ])->validate();
        $category=Category::findOrFail($data['id']);
        $data=$category->childrenRecursiveFlatten();
        if(!empty( $data)){
            Discount::where('modelable_id',  $category->id)->where('modelable_type',Category::class)->delete();
             $item_ids=Item::whereIn('category_id',  $data)->get()->pluck('id')->toArray();
             Item::whereIn('id',  $item_ids)->delete();
             $category->discounts()->delete();
             $category->whereIn('id',  $data)->delete();
        }
       $category=Category::destroy($request->id);
        return  response()->json([
                'message' => __("deleted successfully")
            ], 200);
      
    }
   
}
