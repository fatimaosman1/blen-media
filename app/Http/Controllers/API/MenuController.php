<?php

namespace App\Http\Controllers\API;
use App\Models\Discount;
use App\Models\Item;
use App\Models\Menu;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\JsonResponse;
class MenuController extends Controller
{

    public function ValidationRules($rules = [])
    {
        return array_merge([
            "name" => ['required', 'string'],
        ], $rules);
    }
   
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function listMenu()
    {
        $result=Menu::where('user_id',auth()->user()->id)->with('user:id,name')->get();
        return new JsonResponse($result, 200);
  
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $data = $request->all();
        Validator::make($data, $this->ValidationRules())->validate();
            Menu::create([
                'name' => $data['name'],
                'user_id' =>auth()->user()->id
            ]);
        return new JsonResponse("Success", 200);
    }
    

    public function edit(Request $request)
    {
        $id = $request->id;
        $menu = Menu::findOrFail($id);    
        return new JsonResponse($menu, 200);
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $validated=Validator::make($data, $this->ValidationRules())->validate();
        $menu = Menu::findOrFail($data['id']);
        $menu?->update($validated);
        return new JsonResponse( ['message' => __('updated successfully')], 200);
   
    }
     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return response json
     */
    public function destroy(Request $request)
    {
        $data = $request->all();
        Validator::make($data, [
            'id' => ['required', 'exists:menus,id'],
        ], [
            'id' => __('invalid record selected')
        ])->validate();
        
        $categoris=Category::where('menu_id',$data['id'])->get();
        foreach($categoris as $category){
            $data=$category->childrenRecursiveFlatten();
            if(!empty( $data)){
            
                Discount::where('modelable_id',  $category->id)->where('modelable_type',Category::class)->delete();
                $item_ids=Item::whereIn('category_id',  $data)->get()->pluck('id')->toArray();
                Item::whereIn('id',  $item_ids)->delete();
                $category->discounts()->delete();
                $category->whereIn('id',  $data)->delete();
                }
            }
            Menu::where('id',$request->id)->delete();
     
        return  response()->json([
                'message' => __("deleted successfully")
            ], 200);
      
    }
   


   


}
