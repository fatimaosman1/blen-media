<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\API\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\JsonResponse;

class LoginController extends Controller
{

    

    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password,
        ];
            $token=null;
        if (Auth::attempt($credentials)) {
            $success = true;
            $user = User::where('email', $credentials['email'])->first();
            $token = $user->createToken("authToken")->plainTextToken;
        } else {
            $success = false;
        }

        $response = [
            'success' => $success,
            'access_token' => $token,
        ];
       
        return response()->json($response);
    }
    
    
}
