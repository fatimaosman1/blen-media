<?php

namespace App\Observers;

use App\Models\Category;
use App\Models\Discount;
use App\Models\Item;

class CategoryObserver
{

    public function creating(Category $category){
            if(count($category->parentRecursiveFlatten())>4){
                return false;
            }
    }

    public function deleting (Category $category){
        $data=$category->childrenRecursiveFlatten();
        if(!empty( $data)){
            Discount::where('modelable_id',  $category->id)->where('modelable_type',Category::class)->delete();
             $item_ids=Item::whereIn('category_id',  $data)->get()->pluck('id')->toArray();
             Item::whereIn('id',  $item_ids)->delete();
             $category->discounts()->delete();
             $category->whereIn('id',  $data)->delete();
        }
        }

}
