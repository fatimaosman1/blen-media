<?php

namespace App\Services;


use App\Models\Discount;
use App\Models\Item;
use App\Models\Category;

class DiscountServices 
{

   
    public function discoutMenue($data){
        $result=Category::where('menu_id',$data['model_id'])->whereNull('category_id')->get();
        foreach ($result as $object){
            Discount::create([
                'name'=>$data['name'],
                'value'=>$data['value'],
                'type'=>$data['type'],
                'application'=>$data['application'],
                'status'=>$data['status'],
                'modelable_id'=>$object->id,
                'modelable_type'=>Category::class,
                'user_id' =>auth()->user()->id


            ]);
           
          }
    }
    public function discoutCategory($data){
        $result=Category::where('id',$data['model_id'])->get();
        foreach ($result as $object){
            Discount::create([
                'name'=>$data['name'],
                'value'=>$data['value'],
                'type'=>$data['type'],
                'application'=>$data['application'],
                'status'=>$data['status'],
                'modelable_id'=>$object->id,
                'modelable_type'=>Category::class,
                'user_id' =>auth()->user()->id


            ]);
        }
    }
    public function discoutItem($data){
       
        Discount::create([
            'name'=>$data['name'],
            'value'=>$data['value'],
            'type'=>$data['type'],
            'application'=>$data['application'],
            'status'=>$data['status'],
            'modelable_id'=>$data['model_id'],
            'modelable_type'=>Item::class,
            'user_id' =>auth()->user()->id

        ]);
    }
  

}
