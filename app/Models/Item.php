<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    use HasFactory;

    
    protected $fillable = [
        'name',
        'price',
        'category_id',
        'id'
    ];
    protected $appends = ['new_price'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($item){
           
           $discounts= Discount::whereIn('modelable_id',  $item->id)->where('modelable_type',Item::class)->get();
            foreach($discounts as $dis){
                $dis->delete();
              }
        });
    }
    function discounts()
    {
        return $this->morphMany(Discount::class, 'modelable');
    }
    public function category() 
    {
        return $this->belongsTo(Category::class);
    }
    public function isHaveDiscout()
    {
       $discount= Discount::active()->where('modelable_id',$this->id)->where('modelable_type',Item::class)->get();
       if(count($discount)>0){
        return true;
       }
       return false;
       
    }
    public function getClosetNodeHaveDiscount(){
        if($this->category->isHaveDiscout()){
            return $this->category;
        }else{
            foreach($this->category->parentRecursiveFlatten() as $cat){
                if($cat->isHaveDiscout()){
                    return $cat;
                }

            }
        }
        

    }
    public function getNewPriceAttribute()
    {
        $result=0;
      if($this->isHaveDiscout()){

        foreach ($this->discounts as $object){
            if($object->type=="percentage"){
                $result= $result+(( $object->value * $this->price)/100);
            }else{
                $result= $result+$object->value;
            }
        }
      }else{
        $node= $this->getClosetNodeHaveDiscount();
        if( $node!= null){
            foreach ($node->discounts as $object){
                if($object->type=="percentage"){
                    $result= $result+(( $object->value * $this->price)/100);
                }else{
                    $result= $result+$object->value;
                }
            }
        }
        }
        $result= $this->price - $result;
        return ($result >0) ? $result : 0;
    }
}
