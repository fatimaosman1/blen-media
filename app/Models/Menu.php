<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'name',
        'user_id',
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($menu){
           
            $categoris=Category::where('menu_id',$menu->id)->get();  
            foreach($categoris as $cat){
                $cat->delete();
              }
        });
    }
    public function user() 
    {
        return $this->belongsTo(User::class);
    }
}
