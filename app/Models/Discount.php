<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'value',
        'type',
        'status',
        'application',
        'modelable_id',
        'modelable_type',
        'user_id',
    ];
    
    public function modelable()
    {
        return $this->morphTo();
    }

    public function scopeActive($query){
        return $query->where('status', 'Active');
    }
    
}
