<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
class Category extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'category_id',
        'menu_id',
        'id'
    ];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function($category){
            $data=$category->childrenRecursiveFlatten();
           $discounts= Discount::where('modelable_id',  $category->id)->where('modelable_type',Category::class)->get();
           $items=Item::whereIn('id',$data)->get();
           foreach($discounts as $dis){
                $dis->delete();
              }
            foreach($items as $item){
                $item->delete();
              }  
            $category->whereIn('id',  $data)->delete();
        });
    }

    protected $appends = ['size_parent'];
   /**
     * This will give model's Parent 
     * @return BelongsTo
     */
    public function parent()
    {  
       return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * This will give model's Parent, Parent's parent, and so on until root.  
     * @return BelongsTo
     */
    public function parentRecursive(): BelongsTo
    {
        return $this->parent()->with('parentRecursive');
    }

    /**
     * Get current model's all recursive parents in a collection in flat structure.
     */
    public function parentRecursiveFlatten()
    {
        $result = collect();
        $item = $this->parentRecursive;
        if ($item instanceof Category) {
            $result->push($item);
            $result = $result->merge($item->parentRecursiveFlatten());
        }
        return $result;
    }

    /**
     * This will give model's Children
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(Category::class, 'category_id');
    }

    /**
     * This will give model's Children, Children's Children and so on until last node. 
     * @return HasMany
     */
    public function childrenRecursive(): HasMany
    {
        return $this->children()->with(['childrenRecursive','items']);
    }

      /**
     * Get current model's all recursive children in a collection in flat structure.
     */
    public function childrenRecursiveFlatten()
    {
        $result = array();
        $items = $this->childrenRecursive;
        foreach( $items as $item){
        if ($item instanceof Category) {
            array_push($result,$item->id)  ;;// $result->push($item);
          $result = array_merge($result,$item->childrenRecursiveFlatten());
        }
    }
        return $result;
    }

    public function menu() 
    {
        return $this->belongsTo(Menu::class);
    }

    function discounts()
    {
        return $this->morphMany(Discount::class, 'modelable');
    }
    public function items(): HasMany
    {
        return $this->hasMany(Item::class);
    }

    public function getSizeParentAttribute()
    {
        return count($this->parentRecursiveFlatten());
       
    }
    public function isHaveDiscout()
    {
       $discount= Discount::active()->where('modelable_id',$this->id)->where('modelable_type',Category::class)->get();
       if(count($discount)>0){
        return true;
       }
       return false;
       
    }
}
