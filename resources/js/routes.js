import Vue from 'vue'
import VueRouter from 'vue-router'
import store from './store'

import Menuindex from './Componants/dashboard/Menus/index';
import MenuEdit from './Componants/dashboard/Menus/edit';
import MenuCreate from './Componants/dashboard/Menus/create';
import MenuView from './Componants/dashboard/Menus/view';
import MenuDisplay from './Componants/dashboard/Menus/display';
import Categoryindex from './Componants/dashboard/categories/index';
import DiscountCreate from './Componants/dashboard/discount/create';
import DiscountIndex from './Componants/dashboard/discount/index';
import DiscountEdit from './Componants/dashboard/discount/edit';
import ItemIndex from './Componants/dashboard/items/index';
import App from './Componants/dashboard/welcome.vue';
import Login from './Componants/dashboard/layout/auth/Login';
import Dashboard from './Componants/dashboard/layout/index';
 const routes = [
  
  {
    name:"login",
    path:"/login",
    component:Login,
    meta:{
        middleware:"guest",
        title:`Login`
    }
},
   
   
    {
      path:"/",
      component:Dashboard,
      meta:{
          middleware:"auth"
      },
      children:[
        { name:"dashboard",path: '/dashboard', component: Dashboard, meta:{title:`Dashboard` }},
        { path: '/menu-index', component: Menuindex, name:'menu-index',meta:{title:`Menus` } },
        { path: '/menu-create', component: MenuCreate, name:'menu-create',meta:{title:`Create Menu` } },
        { path: '/menu-edit/:id', component: MenuEdit, name:'menu-edit',meta:{title:`edit Menu` } },
        { path: '/menu-view/:id', component: MenuView, name:'menu-view',meta:{title:`view menue` } },
        { path: '/menu-display/:id', component: MenuDisplay, name:'menu-display',meta:{title:`display menue` } },
        
        { path: '/category-index', component: Categoryindex, name:'category-index',meta:{title:`categories` } },

        { path: '/discount-index', component: DiscountIndex, name:'discount-index',meta:{title:`discount` } },
        { path: '/discount-create', component: DiscountCreate, name:'discount-create',meta:{title:`Create discount` } },
        { path: '/discount-edit/:id', component: DiscountEdit, name:'discount-edit',meta:{title:`edit discount` } },
        { path: '/item-index', component: ItemIndex, name:'item-index',meta:{title:`items` } },
      
        
            
      ]
  }

]







Vue.use(VueRouter);
var router  = new VueRouter({
  mode: 'history',
  routes: routes
})

router.beforeEach((to, from, next) => {
  document.title = `${to.meta.title} - ${process.env.MIX_APP_NAME}`
  if(to.meta.middleware=="guest"){
    if(store.state.auth.authenticated){
      next({name:"menu-index"})
  }
      next()
  }else{
      if(store.state.auth.authenticated){
          next()
      }else{
          next({name:"login"})
      }
  }
})

export default router