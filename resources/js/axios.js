import axios from 'axios'
import Cookies from 'js-cookie'


axios.defaults.headers.common = { 'Authorization': 'Bearer ' + Cookies.get("access_token") }
export default axios.create({
  timeout: 1000,
  headers: { 'Authorization': 'Bearer ' + Cookies.get("access_token") }
})