import './bootstrap';
import Vue from 'vue';//import Vue Libray
import router from './routes'
import store from './store'
import axios from "./axios";
import App from './Componants/dashboard/welcome.vue';
import VueGoodTablePlugin from 'vue-good-table';
import Swal from 'sweetalert2'
import VueTreeList from 'vue-tree-list'
import Cookies from 'js-cookie'

Vue.use(VueTreeList)

// import the styles
import 'vue-good-table/dist/vue-good-table.css'
Vue.use(VueGoodTablePlugin);
Vue.component('Footer', require('./Componants/dashboard/layout/footer').default);
Vue.component('Navbar', require('./Componants/dashboard/layout/navbar').default);
Vue.component('Menu', require('./Componants/dashboard/layout/menu').default);
Vue.component('Login', require('./Componants/dashboard/layout/auth/Login').default);
Vue.component('VueTreeList',  VueTreeList)

const Toast = Swal.mixin({
  toast: true,
  position: 'bottom-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  onOpen: (toast) => {
    toast.addEventListener('mouseenter', Swal.stopTimer)
    toast.addEventListener('mouseleave', Swal.resumeTimer)
  }
});

Vue.prototype.$axios = axios
Vue.prototype.$Toast = Toast

const app = new Vue({
    el :'#app',
    store:store,
    components:{ App },
    router,
})


