<!DOCTYPE html>
<html lang="en" class="light-style layout-navbar-fixed layout-menu-fixed" dir="ltr" data-theme="theme-default" data-assets-path="{{url('dashboard')}}/assets/" data-template="vertical-menu-template">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>@yield('title') - {{env('APP_NAME')}}</title>

    <meta name="description" content="" />



    @include("dashboard.layouts.styles")
 <!-- Page CSS -->
 <link rel="stylesheet" href="{{url('dashboard')}}/assets/vendor/css/pages/cards-advance.css" />
 <!-- Page CSS -->
 <!-- Page -->
 <link rel="stylesheet" href="{{url('dashboard')}}/assets/vendor/css/pages/page-auth.css" />

    <!-- Helpers -->
    <script src="{{url('dashboard')}}/assets/vendor/js/helpers.js"></script>

    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Template customizer: To hide customizer set displayCustomizer value false in config.js.  -->
    <script src="{{url('dashboard')}}/assets/vendor/js/template-customizer.js"></script>
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="{{url('dashboard')}}/assets/js/config.js"></script>
</head>

<body class="antialiased">
        
    <div id="app">
        <router-view></router-view>
    </div>
    @include("dashboard.layouts.scripts")
    <script src="{{ asset('js/app.js') }}"></script>
  
</body>

</html>

