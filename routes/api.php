<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\MenuController;
use App\Http\Controllers\API\ItemController;
use App\Http\Controllers\API\CategoryController;
use App\Http\Controllers\API\DiscountController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => ['auth:sanctum']
], function () {
    Route::get("/list-menu", [MenuController::class, "listMenu"]);
    Route::post("/create-menu", [MenuController::class, "store"]);
    Route::get("/edit-menu/{id}", [MenuController::class, "edit"]);
    Route::post("/edit-menu", [MenuController::class, "update"]);
    Route::post("/delete-menu", [MenuController::class, "destroy"]);

    Route::get("/list-category", [CategoryController::class, "listCategory"]);
    Route::get("/list-categorywithlevel", [CategoryController::class, "listAllLevelCategory"]);
    Route::post("/create-category", [CategoryController::class, "store"]);
    Route::get("/edit-category/{id}", [CategoryController::class, "edit"]);
    Route::post("/edit-category", [CategoryController::class, "update"]);
    Route::post("/delete-category", [CategoryController::class, "destroy"]);

    
    Route::get("/list-item", [ItemController::class, "listItem"]);
    Route::post("/create-item", [ItemController::class, "store"]);
    Route::get("/edit-item/{id}", [ItemController::class, "edit"]);
    Route::post("/edit-item", [ItemController::class, "update"]);
    Route::post("/delete-item", [ItemController::class, "destroy"]);

    Route::get("/list-discount", [DiscountController::class, "listDiscount"]);
    Route::post("/create-discount", [DiscountController::class, "store"]);
    Route::get("/edit-discount/{id}", [DiscountController::class, "edit"]);
    Route::post("/edit-discount", [DiscountController::class, "update"]);
    Route::post("/delete-discount", [DiscountController::class, "delete"]);
 

});

//Route::post("login", [LoginController::class, "login"]);