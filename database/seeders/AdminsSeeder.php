<?php

namespace Database\Seeders;

use App\Models\User;
use Carbon\Carbon;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()


    {


        $admin = new User();

        $admin->name =  'admin';
        $admin->surname = 'admin';
        $admin->username = 'admin';
        $admin->type = 'administration';
        $admin->email = 'admin@admin.com';
        $admin->password = Hash::make('123');
        $admin->email_verified_at = Carbon::now();
        $admin->save();

        $admin->assignRole('Administration');



    }
}
